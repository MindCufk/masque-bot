defmodule MasqueBot.Mixfile do
  use Mix.Project

  def project do
    [
      app: :app,
      version: "0.1.1",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {MasqueBot.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nadia, "~> 0.4.2"},
      {:timex, "~> 3.1"},
      {:distillery, "~> 1.4", runtime: false}
    ]
  end
end
