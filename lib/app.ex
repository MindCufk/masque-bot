defmodule MasqueBot.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(MasqueBot.Poller, []),
      worker(MasqueBot.Matcher, [])
    ]

    options = [strategy: :one_for_one, name: MasqueBot.Supervisor]
    Supervisor.start_link(children, options)
  end
end
