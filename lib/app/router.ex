defmodule MasqueBot.Router do
  @bot_name Application.get_env(:app, :bot_name)

  # Code injectors

  defmacro __using__(_opts) do
    quote do
      require Logger
      import MasqueBot.Router

      def match_message(message) do
        try do
          apply __MODULE__, :do_match_message, [message]
        rescue
          err in FunctionClauseError ->
            Logger.log :warn, """
              Errored when matching command. #{Poison.encode! err}
              Message was: #{Poison.encode! message}
              """
        end
      end
    end
  end

  def generate_message_matcher(handler) do
    quote do
      def do_match_message(var!(update)) do
        handle_message unquote(handler), [var!(update)]
      end
    end
  end

  defp generate_command(command, handler) do
    quote do
      def do_match_message(%{
        message: %{
          text: "/" <> unquote(command)
        }
      } = var!(update)) do
        handle_message unquote(handler), [var!(update)]
      end

      def do_match_message(%{
        message: %{
          text: "/" <> unquote(command) <> " " <> _
        }
      } = var!(update)) do
        handle_message unquote(handler), [var!(update)]
      end

      def do_match_message(%{
        message: %{
          text: "/" <> unquote(command) <> "@" <> unquote(@bot_name)
        }
      } = var!(update)) do
        handle_message unquote(handler), [var!(update)]
      end

      def do_match_message(%{
        message: %{
          text: "/" <> unquote(command) <> "@" <> unquote(@bot_name) <> " " <> _
        }
      } = var!(update)) do
        handle_message unquote(handler), [var!(update)]
      end
    end
  end

  ## Match all
  defmacro message(do: function) do
    generate_message_matcher(function)
  end

  defmacro message(module, function) do
    generate_message_matcher {module, function}
  end

  ## Command

  defmacro command(commands, do: function) when is_list(commands) do
    Enum.map commands, fn command ->
      generate_command(command, function)
    end
  end
  defmacro command(command, do: function) do
    generate_command(command, function)
  end

  defmacro command(commands, module, function) when is_list(commands) do
    Enum.map commands, fn command ->
      generate_command(command, {module, function})
    end
  end

  defmacro command(command, module, function) do
    generate_command(command, {module, function})
  end

  # Helpers

  def handle_message({module, function}, update) when is_atom(function) and is_list(update) do
    apply module, function, [hd update]
    nil
  end
  def handle_message({module, function}, update) when is_atom(function) do
    apply module, function, [update]
  end

  def handle_message(function, update) when is_function(function) do
      function
  end

  def handle_message(_, _), do: nil
end
