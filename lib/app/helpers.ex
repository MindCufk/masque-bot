defmodule MasqueBot.Helpers do
  @bot_name Application.get_env(:app, :bot_name)

  # Code injectors

  defmacro __using__(_opts) do
    quote do
      require Logger
      import MasqueBot.Helpers
      alias Nadia.Model
      alias Nadia.Model.InlineQueryResult
    end
  end
  # Helpers

  defmacro get_chat_id do
    quote do
      case var!(update) do
        %{inline_query: inline_query} when not is_nil(inline_query) ->
          inline_query.from.id
        %{callback_query: callback_query} when not is_nil(callback_query) ->
          callback_query.message.chat.id
        %{message: %{chat: %{id: id}}} when not is_nil(id) ->
          id
        %{edited_message: %{chat: %{id: id}}} when not is_nil(id) ->
          id
        _ -> raise "No chat id found!"
      end
    end
  end
end
