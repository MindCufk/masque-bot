defmodule MasqueBot.Commands.Pidor do
  use MasqueBot.Helpers

  def pidor(update) do
    Logger.log :info, "Command /pidor"
    chat_id_atom = get_chat_id_atom(update)
    current_date_atom = get_current_date_as_atom
    case :ets.lookup(:pidors_history_table, chat_id_atom) do
      [{^chat_id_atom, {^current_date_atom, pidor}}] ->
        Nadia.send_message get_chat_id(), "По результатам сегодняшнего розыгрыша, пидор дня - #{pidor}!"
      _ ->
        pidor =
          :ets.lookup(:users_table, chat_id_atom)
            |> Keyword.get_values(chat_id_atom)
            |> Enum.random

        :ets.insert(:pidors_history_table, {chat_id_atom, {current_date_atom, pidor}})

        data = :ets.lookup(:pidors_table, chat_id_atom)[chat_id_atom] || []
        pidor_atom = String.to_atom(pidor)
        new_data = Keyword.update(data, pidor_atom, 1, &(&1 + 1))

        :ets.insert(:pidors_table, {chat_id_atom, new_data})

        Nadia.send_message get_chat_id(), "@#{pidor} - ты пидор."
    end

  end

  def pidor_stats(update) do
    Logger.log :info, "Command /pidor_stats"
    chat_id_atom = get_chat_id_atom(update)
    all_pidors =
      :ets.lookup(:pidors_table, chat_id_atom)[chat_id_atom]
        |> Enum.map_join("\n", fn {k, v} -> "#{k} — #{v} раз(а)" end)
    Nadia.send_message get_chat_id(), "Пидоры на сегодняшний день: \n" <> all_pidors
  end

  def list_candidates(update) do
    Logger.log :info, "Command /list_candidates"
    chat_id_atom = get_chat_id_atom(update)

    list_of_registered_users =
      :ets.lookup(:users_table, chat_id_atom)
        |> Keyword.get_values(chat_id_atom)
        |> Enum.join(", ")

    Nadia.send_message get_chat_id(), "В данный момент зарегистрированы: " <> list_of_registered_users
  end

  def register(update) do
    Logger.log :info, "Command /register"
    username = update.message.from.username
    chat_id_atom = get_chat_id_atom(update)

    case :ets.lookup(:users_table, chat_id_atom) do
      [{^chat_id_atom, ^username}] ->
        Nadia.send_message get_chat_id(), username <> " уже зарегистрирован."
      _ ->
        :ets.insert(:users_table, {chat_id_atom, username})
        Nadia.send_message get_chat_id(), username <> " зарегистрирован."
    end
  end

  defp get_chat_id_atom(update) do
    get_chat_id()|> Integer.to_string() |> String.to_atom()
  end

  defp get_current_date_as_atom do
    Timex.now("Moscow/Europe")
      |> DateTime.to_date
      |> Date.to_string
      |> String.to_atom
  end
end
