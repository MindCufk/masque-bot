defmodule MasqueBot.Commands do
  use MasqueBot.Router
  use MasqueBot.Helpers
  alias MasqueBot.Commands.Pidor

  #TODO: command "help"
  command "shrug", do: Nadia.send_message get_chat_id(), "¯\\_(ツ)_/¯"
  command "pidor", Pidor, :pidor
  command "pidor_stats", Pidor, :pidor_stats
  command "list_candidates", Pidor, :list_candidates
  command "register", Pidor, :register

  # Catchall matcher
  message do
  end
end
